using Doozy.Engine.UI;
using UnityEngine;

public class LoadingScene : MonoBehaviour {
    static LoadingScene inst;
    public static LoadingScene Inst {
        get {
            if (inst == null) {
                Load();
            }
            return inst;
        }
    }
    public static bool Visible {
        get {
            switch (Inst.View.Visibility) {
                case VisibilityState.Visible:
                case VisibilityState.Hiding:
                    return true;
            }
            return false;
        }
    }
    public static bool IsLoaded {
        get {
            return inst != null;
        }
    }

    public GameObject Content;
    public UIView View;

    private void Awake() {
        inst = this;
    }

    public static void Show() {
        Inst.Content.SetActive(true);
        Inst.View.Show();
    }

    public static void Hide() {
        Inst.View.Hide();
    }

    public static void HideInstant() {
        Inst.Content.SetActive(true);
        Inst.View.Hide(true);
        Inst.Content.SetActive(false);
    }

    public static  void Load() {
        SceneHelper.LoadSceneAdditive(SceneHelper.Scenes.Loading);
    }
}
