using System.Collections;
using System.Runtime.InteropServices;
using UnityEngine;
using UnityEngine.SceneManagement;

public class MainMenu : MonoBehaviour {
#if UNITY_WEBGL
    [DllImport("__Internal")]
    private static extern void Resize100();
#endif
    
    private void Start() {
#if UNITY_WEBGL
        Resize100();
#elif UNITY_ANDROID
        StartCoroutine(ChangeResolutionRoutine());
#endif
    }

    IEnumerator ChangeResolutionRoutine() {
        var resolution = Screen.currentResolution;
        Screen.SetResolution(
            resolution.width - 1,
            resolution.height - 1,
            true,
            resolution.refreshRate
        );
        yield return null;
        Screen.SetResolution(
            resolution.width,
            resolution.height,
            true,
            resolution.refreshRate
        );
    }

    public void StartGame() {
        SceneHelper.LoadScene(SceneHelper.Scenes.Splash);
    }

    public void ExitToDesktop() {
        Application.Quit();
    }
}
