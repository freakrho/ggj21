using System.Collections;
using TMPro;
using UnityEngine;

public class TextAnimation : MonoBehaviour {
    public float FrameLength = .1f;
    public string[] Frames;
    public TMP_Text Text;

    IEnumerator Animation() {
        int index = 0;
        while (true) {
            Text.text = Frames[index];
            yield return new WaitForSecondsRealtime(FrameLength);
            index++;
            if (index >= Frames.Length) {
                index = 0;
            }
        }
    }

    private void Start() {
        StartCoroutine(Animation());
    }
}
