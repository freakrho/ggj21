using System.Collections;
using System.Collections.Generic;
using TMPro;
using UnityEngine;

public class DayText : MonoBehaviour {
    public TMP_Text Text;
    public string Format = "DAY {day}";

    public void SetDay(int day) {
        Text.text = Format.Replace("{day}", day.ToString());
    }
}
