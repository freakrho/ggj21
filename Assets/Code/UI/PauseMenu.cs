using Doozy.Engine.UI;
using UnityEngine;

public class PauseMenu : MonoBehaviour {
    public UIView View;
    public bool Visible { get { return Time.timeScale < 1; } }

    public void ExitPause() {
        View.Hide();
        Time.timeScale = 1;
    }

    public void EnterPause() {
        View.gameObject.SetActive(true);
        View.Show();
        Time.timeScale = 0;
    }

    public void ExitToMenu() {
        SceneHelper.LoadScene(SceneHelper.Scenes.MainMenu);
    }

    public void ExitToDesktop() {
        Application.Quit();
    }
}
