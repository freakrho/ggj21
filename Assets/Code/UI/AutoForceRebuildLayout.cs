using System.Collections;
using UnityEngine;
using UnityEngine.UI;

public class AutoForceRebuildLayout : MonoBehaviour {
    private void OnEnable() {
        StartCoroutine(ForceRebuild());
    }

    IEnumerator ForceRebuild() {
        yield return null;
        yield return null;
        yield return null;
        yield return null;
        yield return null;
        yield return null;
        yield return null;
        LayoutRebuilder.MarkLayoutForRebuild(transform as RectTransform);
    }
}
