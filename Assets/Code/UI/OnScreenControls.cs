using Doozy.Engine.UI;
using UnityEngine;

public class OnScreenControls : MonoBehaviour {
    public static OnScreenControls Inst { get; private set; }

    public UIView View;

    private void Awake() {
        Inst = this;
    }

    private void Start() {
        if (Game.Controller == Game.ControllerType.OnScreen) {
            Show();
        } else {
            Hide();
        }
    }

    public static void Show() {
        Inst.View.Show();
    }

    public static void Hide() {
        Inst.View.Hide();
    }
}
