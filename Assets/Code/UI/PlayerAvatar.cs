using UnityEngine;
using UnityEngine.UI;

public class PlayerAvatar : MonoBehaviour {
    public Image Image;
    public Sprite[] Sprites;

    private void Update() {
        float state = PlayerStats.Inst.Stats.AnimicState /
            PlayerRoom.Inst.StatsConfig.AnimicStateConfig.Max;
        int current = Mathf.RoundToInt((Sprites.Length - 1) * state);
        Image.sprite = Sprites[current];
    }
}