using PixelCrushers.DialogueSystem;
using UnityEngine;

public class Bed : MonoBehaviour {
    private void OnTriggerEnter2D(Collider2D other) {
        if (PlayerRoom.Inst.IsTired()) {
            PlayerRoom.Inst.Sleep();
        } else {
            DialogueManager.StartConversation("NotReadyForBed");
        }
    }
}
