using PixelCrushers.DialogueSystem;
using UnityEngine;
using UnityEngine.Events;

public class PlayerInteraction : MonoBehaviour {
    public DialogueSystemTrigger DialogueTrigger;
    public UnityEvent Action;

    public void OnAction() {
        if (DialogueTrigger != null) {
            DialogueTrigger.OnUse();
        } else {
            Action.Invoke();
        }
    }

    public void OnDialogueEnd() {
        Action.Invoke();
    }
}
