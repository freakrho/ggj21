using UnityEngine;

public class PlayerStats : MonoBehaviour {
    public static PlayerStats Inst;

    public Stats Stats;

    void Awake() {
        Inst = this;
    }
}

[System.Serializable]
public struct Stats {
    public float AnimicState;
    public float Tireing;
    public float DogWalkNeed;
    public float PlantWaterNeed;
    public bool PlantWateredToday;
    public bool DogWalkedToday;
    public int PoopAmount;
    public int GarbageAmount;
    public bool DishesWashed;
}
