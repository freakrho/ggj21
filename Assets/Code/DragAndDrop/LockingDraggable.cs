using UnityEngine;

public class LockingDraggable : Draggable{
    public Transform FigurePlace;
    
    private bool locked = false;
    public override bool Valid => locked;
    
    protected override void LateUpdate(){
        if(locked)  return;

        base.LateUpdate();
    }

    public override void OnMouseDown(){
        if(locked)  return;

        base.OnMouseDown();
    }
    public override void OnMouseUp(){
        if(Mathf.Abs(transform.position.x - FigurePlace.transform.position.x) <= ErrorMargin
            && Mathf.Abs(transform.position.y - FigurePlace.transform.position.y) <= ErrorMargin){
            transform.position = FigurePlace.transform.position;
            locked = true;
        }else{
            transform.position = initialPosition;
        }

        base.OnMouseUp();
    }
}


