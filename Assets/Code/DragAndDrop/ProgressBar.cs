using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class ProgressBar : MonoBehaviour{
    public Slider slider;

    public Transform line1, line2;
        
    public void SetMaxValue(int value){
        slider.maxValue = value * 100;
    }

    public void SetValue(int value){
        slider.value = value * 100;
    }

    public void SetRange(int min, int max){        
        // TODO
    }
}
