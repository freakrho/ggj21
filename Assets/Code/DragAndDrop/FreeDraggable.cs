using UnityEngine;
using UnityEngine.Events;

public class FreeDraggable : Draggable {
    [System.Serializable]
    public class DropEvent : UnityEvent<Vector2> {}
    
    public Collider2D Target;
    public DropEvent OnDrop;

    public override bool Valid => valid;

    Collider2D selfCollider;
    bool valid;

    private void Awake() {
        selfCollider = GetComponent<Collider2D>();
    }

    protected override void LateUpdate() {
        if (valid) return;
        base.LateUpdate();
    }

    public override void OnMouseDown() {
        if (valid) return;
        base.OnMouseDown();
    }

    public override void OnMouseUp() {
        var targetBounds = Target.bounds;
        var selfBounds = selfCollider.bounds;
        targetBounds.extents = targetBounds.extents + Vector3.forward * 100;
        selfBounds.extents = selfBounds.extents + Vector3.forward * 100;
        if (targetBounds.Intersects(selfBounds)) {
            OnDrop.Invoke(transform.position);
            valid = true;
        } else {
            transform.position = initialPosition;
        }
        base.OnMouseUp();
    }
}
