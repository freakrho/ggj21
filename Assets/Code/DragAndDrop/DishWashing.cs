using UnityEngine;
using UnityEngine.Events;

public class DishWashing : MonoBehaviour {
    public int StepsMin = 4;
    public int StepsMax = 7;
    public SteppedDraggable Draggable;

    private void Start() {
        Draggable.Goal = Random.Range(StepsMin, StepsMax + 1);
    }
}
