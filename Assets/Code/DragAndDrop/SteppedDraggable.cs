using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SteppedDraggable : Draggable{
    public List<GameObject> Stops;
    public List<Animator> StopResults;
    public float Cooldown = 1;
    public override bool Valid => Counter >= Goal;
    private int stepsTaken = 0;
    private bool waiting = false;
    public bool GoToBaseOnComplete = true;
    public int Counter = 0;
    public int Goal = 10;
    public Animator CounterAnimator;

    protected override void LateUpdate(){
        if(waiting)  return;

        base.LateUpdate();
    }

    public override void OnMouseDown(){
        if(stepsTaken >= Stops.Count)   return;
        if(waiting)  return;

        base.OnMouseDown();
    }

    public override void OnMouseUp(){
        if(stepsTaken >= Stops.Count)   return;
        var nextStop = Stops[stepsTaken];

        if(Mathf.Abs(transform.position.x - nextStop.transform.position.x) <= ErrorMargin
            && Mathf.Abs(transform.position.y - nextStop.transform.position.y) <= ErrorMargin){
            transform.position = nextStop.transform.position;
            waiting = true;
            StopResults[stepsTaken].SetBool("Active", true);
            stepsTaken++;
            

            if(stepsTaken < Stops.Count + 1){
                StartCoroutine(CooldownRoutine());
            }
        }else{
            if(stepsTaken > 0){
                var previousStop = Stops[stepsTaken-1];
                transform.position = previousStop.transform.position;
            }else{
                transform.position = initialPosition;
            }
        }

        base.OnMouseUp();
    }

    IEnumerator CooldownRoutine(){
        yield return new WaitForSeconds(Cooldown);
        
        if(stepsTaken >= 1){
            StopResults[stepsTaken-1].SetBool("Active", false);
        }
        
        if(GoToBaseOnComplete && stepsTaken >= Stops.Count){
            yield return new WaitForSeconds(1f);
            Counter++;
            CounterAnimator.SetInteger("Level", Mathf.CeilToInt((Goal-Counter)/3) + 1);

            if(Valid){
                CounterAnimator.gameObject.SetActive(false);
                DragAnDropController.Instance.CheckCompleted();
            }

            transform.position = initialPosition;
            stepsTaken = 0;
        }
        waiting = false;
    }

}
