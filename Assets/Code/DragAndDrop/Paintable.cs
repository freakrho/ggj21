using UnityEngine;
using UnityEngine.InputSystem;

public class Paintable : MonoBehaviour {
    public PaintArea[] Areas;
    public SpriteRenderer Renderer;

    Draggable draggable;

    private void Awake() {
        draggable = GetComponent<Draggable>();
    }

    private void LateUpdate() {
        if (!draggable.dragging) return;

        var mousePosition = new Vector3(Mouse.current.position.x.ReadValue(), Mouse.current.position.y.ReadValue(), 0);
        mousePosition = DragAnDropController.Instance.Camera.ScreenToWorldPoint(mousePosition);

        for (int i = 0; i < Areas.Length; i++) {
            var area = Areas[i];
            var rect = new Rect(area.Collider.bounds.min, area.Collider.bounds.size);
            if (rect.Contains(mousePosition)) {
                Renderer.color = area.Color;
            }
        }
    }
}
