using UnityEngine;

public class PaintArea : MonoBehaviour {
    public Color Color;
    public Collider2D Collider;

    private void Awake() {
        Collider = GetComponent<Collider2D>();
    }
}
