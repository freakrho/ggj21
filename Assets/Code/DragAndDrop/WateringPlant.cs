using UnityEngine;
using UnityEngine.Events;

public class WateringPlant : MonoBehaviour {
    public UnityEvent OnEnd;
    
    public void WaterPlantsOK() {
        PlayerStats.Inst.Stats.PlantWateredToday = true;
        PlayerStats.Inst.Stats.PlantWaterNeed = 0;
        OnEnd.Invoke();
    }

    public void WaterPlantsWrong() {
        PlayerStats.Inst.Stats.PlantWateredToday = true;
        OnEnd.Invoke();
    }
}
