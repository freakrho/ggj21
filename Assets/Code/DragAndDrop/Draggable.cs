using UnityEngine.InputSystem;
using UnityEngine;

public class Draggable : MonoBehaviour{    
    protected Vector2 initialPosition;    
    protected float deltaX, deltaY;
    public bool dragging = false;
    public virtual bool Valid => true;
    public float ErrorMargin = 0.5f;    
    protected virtual void Start(){
        initialPosition = transform.position;
    }

    protected virtual void LateUpdate(){
        if(dragging){
            var mousePosition = new Vector3(Mouse.current.position.x.ReadValue(), Mouse.current.position.y.ReadValue(), 0);
            mousePosition = DragAnDropController.Instance.Camera.ScreenToWorldPoint(mousePosition);
            transform.position = new Vector3(mousePosition.x - deltaX, mousePosition.y - deltaY, transform.position.z);
        }
    }

    public virtual void OnMouseDown(){
        if (dragging) return;

        var mousePosition = new Vector3(Mouse.current.position.x.ReadValue(), Mouse.current.position.y.ReadValue(), 0);
        mousePosition = DragAnDropController.Instance.Camera.ScreenToWorldPoint(mousePosition);

        deltaX = mousePosition.x - transform.position.x;
        deltaY = mousePosition.y - transform.position.y;
        dragging = true;
    }
    public virtual void OnMouseUp(){
        dragging = false;
        DragAnDropController.Instance.CheckCompleted();
    }
}
