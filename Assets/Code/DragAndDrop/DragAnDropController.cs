using UnityEngine.InputSystem;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;

public class DragAnDropController : MonoBehaviour{
    public UnityEvent OnComplete;
    public Camera Camera;
    public List<Draggable> Draggables;
    private static DragAnDropController instance = null;
    public static DragAnDropController Instance{ 
        get{
            return instance;
        }
    }

    private Draggable drag;
    void Awake(){
        instance = this;
    }

    private void Start() {
        PlayerRoom.Inst.SetPause(true);
    }

    public virtual bool CheckCompleted(){
        for(var i = 0; i < Draggables.Count; ++i){
            if(!Draggables[i].Valid)   return false;
        }
        OnComplete.Invoke();
        
        return true;
    }

    void LateUpdate(){
        if(Mouse.current.leftButton.IsPressed()){
            Vector3 position = new Vector3(Mouse.current.position.x.ReadValue(), Mouse.current.position.y.ReadValue(), 0);
            position = Camera.ScreenToWorldPoint(position);

            RaycastHit2D hit = Physics2D.Raycast(position, Camera.transform.forward);
            if (hit.collider != null){
                var go = hit.collider.gameObject;
                var draggable = go.GetComponent<Draggable>();
                if(draggable != null){
                    draggable.OnMouseDown();
                    drag = draggable;
                }
            }
        }else{
            if(drag != null){
                drag.OnMouseUp();
                drag = null;
            }
        }
    }

    public void GoBackToRoom() {
        PlayerRoom.Inst.GoBackToRoom(gameObject.scene);
    }
}

