using System.Collections;
using System.IO;
using UnityEngine;
using UnityEngine.UI;

public class DrawingController : MonoBehaviour {
    public GameObject[] SpawnParticles;
    public Collider2D DrawArea;
    public Camera Camera;
    public RawImage Image;

    public void OnDrop(Vector2 position) {
        var obj = CFX_SpawnSystem.GetNextObject(
            SpawnParticles[Random.Range(0, SpawnParticles.Length)]);
        obj.transform.position = position;
    }

    public void End() {
        StartCoroutine(CaptureDrawing());
    }

    public void GoBackToRoom() {
        PlayerRoom.Inst.EndDrawing();
        PlayerRoom.Inst.GoBackToRoom(gameObject.scene);
    }

    IEnumerator CaptureDrawing() {
        yield return new WaitForEndOfFrame();

        var bounds = DrawArea.bounds;
        var min = Camera.WorldToScreenPoint(bounds.min);
        var max = Camera.WorldToScreenPoint(bounds.max);
        var minPos = Vector2.Min(min, max);
        var maxPos = Vector2.Max(min, max);
        var tex = new Texture2D(
            Mathf.FloorToInt(maxPos.x - minPos.x),
            Mathf.FloorToInt(maxPos.y - minPos.y),
            TextureFormat.RGB24, false);

        tex.ReadPixels(new Rect(minPos, maxPos - minPos), 0, 0);
        tex.Apply();
        PersistentData.Drawing = tex;
        Image.texture = tex;

        var bytes = tex.EncodeToPNG();
        var path = Application.persistentDataPath
            + $"/Drawing-{System.DateTime.Now.ToString("yyyy-MM-dd_HH-mm-ss")}.png";
        File.WriteAllBytes(path, bytes);
        Debug.Log($"Drawing saved to {path}");
    }
}
