using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;

public class TimedDraggable : Draggable{
    public float MinTime;
    public float MaxTime;
    public int MaxAttempts = 1;
    public GameObject Destination;
    private float timeLapsed;
    private bool arrived;
    private int attempts;
    public override bool Valid => timeLapsed <= MaxTime && timeLapsed >= MinTime;
    public UnityEvent OnWin;
    public UnityEvent OnLose;

    public ProgressBar ProgressBar;
    public Animator ResultAnimator;
    public Animator SelfAnimator;

    public AudioClip FailAudio;
    public AudioClip WinAudio;

    protected override void Start(){
        base.Start();
        ProgressBar.SetMaxValue((int)MaxTime + 1);
        ProgressBar.SetRange((int)MinTime, (int)MaxTime);
    }
    public override void OnMouseDown(){
        if(attempts >= MaxAttempts) return;
        SelfAnimator?.SetBool("Watering", false);
        base.OnMouseDown();
    }
    public override void OnMouseUp() {
        dragging = false;

        if(arrived){
            transform.position = initialPosition;
            Destination.gameObject.SetActive(!Valid);
            arrived = false;            
            attempts++;
        }

        if(!arrived && Mathf.Abs(transform.position.x - Destination.transform.position.x) <= ErrorMargin
            && Mathf.Abs(transform.position.y - Destination.transform.position.y) <= ErrorMargin){
                
            transform.position = Destination.transform.position;
            Destination.gameObject.SetActive(false);
            arrived = true;
            SelfAnimator?.SetBool("Watering", true);
            timeLapsed = 0;
        }

        if(!Valid && attempts >= MaxAttempts){
            ResultAnimator?.SetTrigger("Fail");
            SoundManager.Inst.Play(FailAudio);
            OnLose.Invoke();
        }

        if(Valid){
            ResultAnimator?.SetTrigger("Win");
            SoundManager.Inst.Play(WinAudio);
            DragAnDropController.Instance.CheckCompleted();
            OnWin.Invoke();
        }
    }
    void Update(){
        if(arrived){
            timeLapsed += Time.deltaTime;
            ProgressBar.SetValue((int)timeLapsed);
        }
    }
}
