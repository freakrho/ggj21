using UnityEngine;
using UnityEngine.Events;

public class SpriteButton : MonoBehaviour {
    public Sprite SpritePressed;
    public UnityEvent OnClick;

    Sprite spriteNormal;
    SpriteRenderer spriteRenderer;
    bool pressed;

    private void Awake() {
        spriteRenderer = GetComponent<SpriteRenderer>();
        spriteNormal = spriteRenderer.sprite;
    }

    void OnMouseDown() {
        if (pressed) return;

        pressed = true;
        spriteRenderer.sprite = SpritePressed;
    }

    void OnMouseUp() {
        if (pressed) {
            spriteRenderer.sprite = spriteNormal;
            OnClick.Invoke();
        }
    }

    void OnMouseExit() {
        pressed = false;
        spriteRenderer.sprite = spriteNormal;
    }

    void OnMouseEnter() {

    }
}
