using System.Collections;
using UnityEngine;
using UnityEngine.SceneManagement;

public class SceneHelper : MonoBehaviour {
    const float MIN_LOADING_TIME = .5f;

    public enum Scenes {
        Room,
        Drawing,
        Loading,
        Splash,
        WateringPlant,
        Computer,
        MainMenu,
        Dishes
    }

    static SceneHelper inst;
    public static SceneHelper Inst {
        get {
            if (inst == null) {
                inst = FindObjectOfType<SceneHelper>();
            }
            if (inst == null) {
                inst = new GameObject("SceneHelper").AddComponent<SceneHelper>();
            }
            return inst;
        }
    }

    IEnumerator ShowLoading() {
        if (!LoadingScene.IsLoaded) {
            LoadingScene.Load();
            while (!LoadingScene.IsLoaded) yield return null;
        }
        LoadingScene.Show();
        while (!LoadingScene.Visible) yield return null;
    }

    IEnumerator LoadSceneAsync(
        Scenes scene,
        System.Action showLoadingCallback,
        System.Action hideLoadingCallback
    ) {
        var loading = ShowLoading();
        while (loading.MoveNext()) yield return null;

        showLoadingCallback?.Invoke();

        var operation = SceneManager.LoadSceneAsync(SceneList.GetSceneIndex(scene),
            LoadSceneMode.Additive);
        float timeStart = Time.time;
        while (!operation.isDone) {
            yield return null;
        }
        // Make sure min loading time
        while (Time.time - timeStart < MIN_LOADING_TIME) yield return null;

        LoadingScene.Hide();
        while (LoadingScene.Visible) yield return null;
        hideLoadingCallback?.Invoke();
    }

    IEnumerator UnloadSceneAsync(
        Scene scene,
        System.Action showLoadingCallback,
        System.Action hideLoadingCallback
    ) {
        var loading = ShowLoading();
        while (loading.MoveNext()) yield return null;
        
        showLoadingCallback?.Invoke();

        var operation = SceneManager.UnloadSceneAsync(scene);
        while (!operation.isDone) {
            yield return null;
        }
        LoadingScene.Hide();
        while (LoadingScene.Visible) yield return null;
        hideLoadingCallback?.Invoke();
    }

    public static AsyncOperation LoadSceneAsyncRaw(Scenes scene, LoadSceneMode mode) {
        return SceneManager.LoadSceneAsync(SceneList.GetSceneIndex(scene), mode);
    }

    public static void LoadScene(Scenes scene) {
        Debug.Log($"Loading scene {scene}");
        SceneManager.LoadScene(SceneList.GetSceneIndex(scene));
    }

    public static void LoadSceneAdditive(Scenes scene) {
        SceneManager.LoadSceneAsync(
            SceneList.GetSceneIndex(scene), LoadSceneMode.Additive);
    }

    public static void LoadSecondaryScene(
        Scenes scene,
        System.Action showLoadingCallback = null,
        System.Action hideLoadingCallback = null
    ) {
        Inst.StartCoroutine(Inst.LoadSceneAsync(scene, showLoadingCallback, hideLoadingCallback));
    }

    public static void UnloadSecondaryScene(
        Scene scene,
        System.Action showLoadingCallback = null,
        System.Action hideLoadingCallback = null
    ) {
        Inst.StartCoroutine(Inst.UnloadSceneAsync(scene, showLoadingCallback, hideLoadingCallback));
    }
}
