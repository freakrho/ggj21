using UnityEngine;

[ExecuteInEditMode]
public class SpriteZPosition : MonoBehaviour {
    private void Update() {
        transform.position = new Vector3(
            transform.position.x,
            transform.position.y,
            transform.position.y
        );
    }
}