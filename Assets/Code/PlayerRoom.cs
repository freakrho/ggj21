using UnityEngine;
using PixelCrushers.DialogueSystem;
using UnityEngine.Playables;
using UnityEngine.Serialization;

public class PlayerRoom : MonoBehaviour {
    public static PlayerRoom Inst { get; private set; }
    public bool Pause { get; private set; }
    public int CurrentDay { get; private set; }

    public StatsConfig StatsConfig;
    public Player Player;
    public float GoodAnimicStateSimTime = 10 * 60;
    public PauseMenu PauseMenu;
    [Header("Room")]
    [Header("Locations")]
    [FormerlySerializedAs("Content")]
    public SlotSpawner PoopSpawner;
    public GameObject RoomContent;
    public Transform RoomPlayerSpawn;
    public Dog Dog;
    public PlayableDirector InspirationTimeline;
    [Header("Park")]
    public GameObject ParkContent;
    public Transform ParkPlayerSpawn;
    public DayText DayText;
    public PlayableDirector NewDayTimeline;

    public event System.Action OnSleep;
    public event System.Action OnWashDishes;

    bool drewOnce;

    private void Awake() {
        Inst = this;
        SetRoomActive(true);
    }

    private void Start() {
        PlayerStats.Inst.Stats = StatsConfig.StartingStats;
        CurrentDay = 1;
    }

    private void Update() {
        PlayerStats.Inst.Stats.PoopAmount = PoopSpawner.Count;
        PlayerStats.Inst.Stats.GarbageAmount = FindObjectsOfType<Garbage>().Length
            - PlayerStats.Inst.Stats.PoopAmount;
        StatsConfig.AdvanceStats(ref PlayerStats.Inst.Stats, Time.deltaTime);
        if (PlayerStats.Inst.Stats.AnimicState < 0) {
            PlayerStats.Inst.Stats.AnimicState = 0;
        } else if (PlayerStats.Inst.Stats.AnimicState > StatsConfig.AnimicStateConfig.Max) {
            PlayerStats.Inst.Stats.AnimicState = StatsConfig.AnimicStateConfig.Max;
        }
    }

    public bool IsTired() {
        return PlayerStats.Inst.Stats.Tireing >= StatsConfig.TireingConfig.CriticalValue;
    }

    bool CheckForActivity() {
        if (IsTired()) {
            DialogueManager.Instance.StartConversation("Tired");
            return true;
        }
        return CheckForPoop();
    }

    bool CheckForPoop() {
        if (!PoopSpawner.Cleared) {
            DialogueLua.SetVariable("Random", Random.Range(0, 4));
            DialogueManager.Instance.StartConversation("PoopOnFloor");
            return true;
        }
        return false;
    }

    void ChangeTireing(float value) {
        PlayerStats.Inst.Stats.Tireing += value;
        if (PlayerStats.Inst.Stats.Tireing >= StatsConfig.TireingConfig.Max) {
            PlayerStats.Inst.Stats.Tireing = StatsConfig.TireingConfig.Max;
        } else if (PlayerStats.Inst.Stats.Tireing < 0) {
            PlayerStats.Inst.Stats.Tireing = 0;
        }
    }

    bool DogNeedsWalk() {
        return PlayerStats.Inst.Stats.DogWalkNeed >= StatsConfig.DogWalkNeedConfig.CriticalValue;
    }

    public void Sleep() {
        if (CheckForPoop()) return;
        if (DogNeedsWalk()) {
            DialogueManager.Instance.StartConversation("SleepDogBark");
        }

        CurrentDay++;
        Debug.Log($"Day {CurrentDay}");
        DayText.SetDay(CurrentDay);
        NewDayTimeline.Play();
        ChangeTireing(-StatsConfig.TireingConfig.Speed);

        PlayerStats.Inst.Stats.PlantWateredToday = false;
        PlayerStats.Inst.Stats.DogWalkedToday = false;
        PlayerStats.Inst.Stats.DishesWashed = false;

        OnSleep?.Invoke();
    }

    public void CleanedGarbage() {
        ChangeTireing(StatsConfig.CleanGarbageTireAmount);
    }

    // Activities
    public void StartDrawingDialogue() {
        if (CheckForActivity()) return;

        DialogueManager.Instance.StartConversation("Drawing");
    }

    public void DogDialogue() {
        if (CheckForPoop()) return;

        DialogueManager.Instance.StartConversation("Dog");
    }

    public void WaterPlantDialogue() {
        if (CheckForActivity()) return;
        if (PlayerStats.Inst.Stats.PlantWateredToday) {
            DialogueManager.Instance.StartConversation("PlantAlreadyWatered");
            return;
        }

        DialogueManager.Instance.StartConversation("WaterPlant");
    }

    public void WashDishesDialogue() {
        if (PlayerStats.Inst.Stats.DishesWashed) return;
        DialogueManager.Instance.StartConversation("WashDishes");
    }

    public void StartDrawing() {
        PlayerStats.Inst.Stats.AnimicState -= StatsConfig.DrawingAnimicBoost;
        SceneHelper.LoadSecondaryScene(SceneHelper.Scenes.Drawing, () => { SetRoomActive(false); });
        ChangeTireing(StatsConfig.DrawingTireAmount);
    }

    public void WaterPlant() {
        SceneHelper.LoadSecondaryScene(SceneHelper.Scenes.WateringPlant, () => { SetRoomActive(false); });
        ChangeTireing(StatsConfig.WateringPlantTireAmount);
    }

    public void WashDishes(){
        SceneHelper.LoadSecondaryScene(SceneHelper.Scenes.Dishes, () => { SetRoomActive(false); });
        PlayerStats.Inst.Stats.DishesWashed = true;
        ChangeTireing(StatsConfig.WashDishesTireAmount);
        OnWashDishes?.Invoke();
    }

    public void WalkDog() {
        if (PlayerStats.Inst.Stats.DogWalkedToday && !DogNeedsWalk()) {
            DialogueManager.Instance.StopConversation();
            DialogueManager.Instance.StartConversation("DogWalkedToday");
            Dog.Cancel();
            return;
        }

        SetParkActive(true);
        PlayerStats.Inst.Stats.DogWalkNeed = 0;
        PlayerStats.Inst.Stats.DogWalkedToday = true;
        PlayerStats.Inst.Stats.AnimicState -= StatsConfig.DogWalkAnimicBoost;
    }

    public void EndWalkDog() {
        SetRoomActive(true, true);
        PlayerStats.Inst.Stats.DogWalkNeed = 0;
        ChangeTireing(StatsConfig.WalkDogTireAmount);
    }

    public void EndDrawing() {
        SetPause(false);
        if (!drewOnce) {
            Debug.Log(" PLAYING TIMELINE");
            InspirationTimeline.Play();
            drewOnce = true;
        }
    }

    public void GoBackToRoom(UnityEngine.SceneManagement.Scene scene) {
        SceneHelper.UnloadSecondaryScene(scene, () => { SetRoomActive(true); });
    }

    public void SetRoomActive(bool value, bool useSpawn = false) {
        SetPause(false);
        if (value && useSpawn) {
            Player.transform.position = RoomPlayerSpawn.position;
        }
        RoomContent.SetActive(value);
        ParkContent.SetActive(!value);
    }

    public void SetParkActive(bool value) {
        if (value) {
            Player.transform.position = ParkPlayerSpawn.position;
        }
        ParkContent.SetActive(value);
        RoomContent.SetActive(!value);
    }

    public void SetPause(bool value) {
        Pause = value;
    }

    public void SpawnPoop() {
        PoopSpawner.Spawn();
    }

    public void OnDialogueEnd() {
        if (FindObjectOfType<DragAnDropController>() == null) {
            SetPause(false);
        }
    }
}
