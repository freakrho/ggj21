using UnityEngine;
using UnityEditor;

public class SpritePreProcessor : AssetPostprocessor {
    void OnPreprocessTexture() {
        if (!assetPath.Contains("Sprites/PixelArt")) return;

        var importer = AssetImporter.GetAtPath(assetPath) as TextureImporter;
        importer.spritePixelsPerUnit = 32;
        importer.filterMode = FilterMode.Point;
        importer.textureCompression = TextureImporterCompression.Uncompressed;
    }
}
