public static class ArrayUtils {
    public static bool Contains(this string[] list, string item) {
        for (int i = 0; i < list.Length; i++) {
            if (list[i] == item) return true;
        }
        return false;
    }
}
