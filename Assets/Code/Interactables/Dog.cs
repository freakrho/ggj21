using PixelCrushers.DialogueSystem;
using UnityEngine;
using UnityEngine.Playables;

public class Dog : MonoBehaviour {
    static readonly int AWAKE = Animator.StringToHash("Awake");
    static readonly int CRITICAL = Animator.StringToHash("Critical");
    static readonly int DIR_X = Animator.StringToHash("DirX");
    static readonly int DIR_Y = Animator.StringToHash("DirY");

    public PlayableDirector PetTimeline;
    public Animator Animator;
    
    bool critical;

    private void Start() {
        PetTimeline.stopped += (director) => {
            Animator.SetBool(AWAKE, false);
            PetTimeline.timeUpdateMode = DirectorUpdateMode.Manual;
            PetTimeline.time = 0;
            PetTimeline.Evaluate();
            PetTimeline.timeUpdateMode = DirectorUpdateMode.GameTime;
        };
    }

    private void Update() {
        critical =
            PlayerStats.Inst.Stats.DogWalkNeed >= PlayerRoom.Inst.StatsConfig.DogWalkNeedConfig.CriticalValue;
        if (PlayerStats.Inst.Stats.DogWalkNeed >= PlayerRoom.Inst.StatsConfig.DogWalkNeedConfig.Max) {
            // Maxed, release poop
            PlayerStats.Inst.Stats.DogWalkNeed = PlayerRoom.Inst.StatsConfig.DogWalkNeedConfig.CriticalValue;
            PlayerRoom.Inst.SpawnPoop();
        }
    }

    private void FixedUpdate() {
        Animator.SetBool(CRITICAL, critical);
    }

    public void Use() {
        PlayerRoom.Inst.DogDialogue();
        Animator.SetBool(AWAKE, true);
    }

    public void Pet() {
        PetTimeline.Play();
        var dir = (PlayerRoom.Inst.Player.transform.position - transform.position).normalized;
        Animator.SetFloat(DIR_X, dir.x);
        Animator.SetFloat(DIR_Y, dir.y);
    }

    public void Walk() {
        PlayerRoom.Inst.WalkDog();
    }

    public void Cancel() {
        Animator.SetBool(AWAKE, false);
    }
}
