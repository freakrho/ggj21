using UnityEngine;

public class Sink : MonoBehaviour {
    static readonly int ON = Animator.StringToHash("ON");
    static readonly int FULL = Animator.StringToHash("Full");
    
    public Animator Animator;

    private void OnEnable() {
        UpdateDishes();
        PlayerRoom.Inst.OnSleep -= UpdateDishes;
        PlayerRoom.Inst.OnSleep += UpdateDishes;
        PlayerRoom.Inst.OnWashDishes -= UpdateDishes;
        PlayerRoom.Inst.OnWashDishes += UpdateDishes;
    }

    private void OnDisable() {
        PlayerRoom.Inst.OnSleep -= UpdateDishes;
        PlayerRoom.Inst.OnWashDishes -= UpdateDishes;
    }

    void UpdateDishes() {
        Animator.SetBool(FULL, !PlayerStats.Inst.Stats.DishesWashed);
    }

    void OnUse() {
        Animator.SetBool(ON, true);
    }

    void OnDeselect() {
        Animator.SetBool(ON, false);
    }
}
