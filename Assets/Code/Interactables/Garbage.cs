using UnityEngine;

public class Garbage : MonoBehaviour {
    public GameObject Particles;
    public AudioClip Clip;

    public void Dispose() {
        var ps = CFX_SpawnSystem.GetNextObject(Particles);
        ps.transform.position = transform.position;
        PlayerRoom.Inst.CleanedGarbage();
        SoundManager.Inst.Play(Clip);
        Destroy(gameObject);
    }
}
