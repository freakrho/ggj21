using PathCreation;
using PixelCrushers.DialogueSystem;
using UnityEngine;
using UnityEngine.Playables;

public class ParkDog : MonoBehaviour {
    static readonly int AWAKE = Animator.StringToHash("Awake");
    static readonly int DIR_X = Animator.StringToHash("DirX");
    static readonly int DIR_Y = Animator.StringToHash("DirY");
    static readonly int VEL_X = Animator.StringToHash("VelX");
    static readonly int VEL_Y = Animator.StringToHash("VelY");
    static readonly int VEL = Animator.StringToHash("Vel");

    enum State {
        Walking,
        Idle,
    }

    public PathCreator[] Paths;
    public float SpeedMax = 5f;
    public float SpeedMin = 1f;
    public Animator Animator;
    public float PathTimeMin = 5;
    public float PathTimeMax = 15;
    public PlayableDirector PetTimeline;

    State state;
    PathCreator currentPath;
    bool inPath;
    float pathPosition;
    Vector2 start;
    Vector2 target;
    float moveTime;
    float stateTime = 0;
    float pathTime;
    float speed;

    private void Awake() {
        PetTimeline.stopped += OnEndPet;
    }

    private void OnEndPet(PlayableDirector obj) {
        SetState(State.Walking);
        PetTimeline.timeUpdateMode = DirectorUpdateMode.Manual;
        PetTimeline.time = 0;
        PetTimeline.Evaluate();
        PetTimeline.timeUpdateMode = DirectorUpdateMode.GameTime;
    }

    private void OnEnable() {
        SetState(State.Walking);
        Animator.SetBool(AWAKE, true);
    }

    void SetState(State state) {
        this.state = state;

        if (state == State.Walking) {
            speed = Random.Range(SpeedMin, SpeedMax);
            currentPath = Paths[Random.Range(0, Paths.Length)];
            inPath = false;
            pathPosition = Random.Range(0f, currentPath.path.length);
            start = transform.position;
            target = currentPath.path.GetPointAtDistance(pathPosition);
            moveTime = (target - start).magnitude / speed;
            pathTime = Random.Range(PathTimeMin, PathTimeMax);
        } else if (state == State.Idle) {

        }

        stateTime = 0;
    }

    private void Update() {
        if (state == State.Walking) {
            if (inPath) {
                pathPosition += speed * Time.deltaTime;
                transform.position = currentPath.path.GetPointAtDistance(pathPosition);
                var dir = currentPath.path.GetDirectionAtDistance(pathPosition);
                Animator.SetFloat(DIR_X, dir.x);
                Animator.SetFloat(DIR_Y, dir.y);
                dir *= speed;
                Animator.SetFloat(VEL_X, dir.x);
                Animator.SetFloat(VEL_Y, dir.y);
            } else {
                transform.position = Vector2.Lerp(start, target, stateTime / moveTime);
                var dir = (target - start).normalized;
                Animator.SetFloat(DIR_X, dir.x);
                Animator.SetFloat(DIR_Y, dir.y);
                dir *= speed;
                Animator.SetFloat(VEL_X, dir.x);
                Animator.SetFloat(VEL_Y, dir.y);
                if (stateTime >= moveTime) {
                    inPath = true;
                }                
            }
            Animator.SetFloat(VEL, speed);

            if (stateTime > pathTime) {
                SetState(State.Walking);
            }
        } else if (state == State.Idle) {
            Animator.SetFloat(VEL, 0);
        }
        stateTime += Time.deltaTime;
    }

    public void OnUse(Transform actor) {
        var dir = (actor.position - transform.position).normalized;
        Animator.SetFloat(DIR_X, dir.x);
        Animator.SetFloat(DIR_Y, dir.y);

        SetState(State.Idle);

        DialogueManager.StartConversation("DogInPark");
    }
}
