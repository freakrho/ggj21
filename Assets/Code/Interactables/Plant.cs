using UnityEngine;

public class Plant : MonoBehaviour {
    public SpriteRenderer Renderer;
    public Sprite[] StateSprites;

    private void Update() {
        float state = PlayerStats.Inst.Stats.PlantWaterNeed /
            PlayerRoom.Inst.StatsConfig.PlantWaterNeedConfig.Max;
        int current = Mathf.RoundToInt(StateSprites.Length * state);
        Renderer.sprite = StateSprites[current];
    }
}
