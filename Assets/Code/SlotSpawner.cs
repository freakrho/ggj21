using UnityEngine;

public class SlotSpawner : MonoBehaviour {
    public Transform[] Slots;
    public GameObject Prefab;

    public bool Cleared {
        get {
            for (int i = 0; i < spawned.Length; i++) {
                if (spawned[i] != null) return false;
            }
            return true;
        }
    }

    public int Count {
        get {
            int count = 0;
            for (int i = 0; i < spawned.Length; i++) {
                if (spawned[i] != null) {
                    count++;
                }
            }
            return count;
        }
    }

    GameObject[] spawned;

    private void Awake() {
        spawned = new GameObject[Slots.Length];
    }

    public void Spawn() {
        for (int i = 0; i < spawned.Length; i++) {
            if (spawned[i] == null) {
                var spawn = Instantiate(Prefab, Slots[i]);
                spawned[i] = spawn;
                return;
            }
        }
    }
}
