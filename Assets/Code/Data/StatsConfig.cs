using UnityEngine;

[CreateAssetMenu(fileName = "StartingStats", menuName = "ggj21/StartingStats", order = 0)]
public class StatsConfig : ScriptableObject {
    public Stats StartingStats;
    public NeedConfig AnimicStateConfig;
    public NeedConfig TireingConfig;
    public NeedConfig DogWalkNeedConfig;
    public NeedConfig PlantWaterNeedConfig;
    
    [Header("Animic modifiers")]
    public float TireingAnimicModifier;
    public float DogAnimicModifier;
    public float PlantAnimicModifier;
    public float DirtyDishesAnimicModifier;
    public float GarbageAnimicModifier;
    public float PoopAnimicModifier;
    public float DrawingAnimicBoost = 40;
    public float DogWalkAnimicBoost = 40;

    [Header("Tire amounts")]
    public float DrawingTireAmount;
    public float WateringPlantTireAmount;
    public float WalkDogTireAmount;
    public float UseComputerTireAmount;
    public float WashDishesTireAmount;
    public float CleanGarbageTireAmount;

    public float GoodCommentBoost = 1f;
    public float NegativeCommentBoost = -1f;

    public void AdvanceStats(ref Stats stats, float deltaTime) {
        AnimicStateConfig.Tick(deltaTime, ref stats.AnimicState);
        var modifier = PoopAnimicModifier * stats.PoopAmount
            + GarbageAnimicModifier * stats.PoopAmount
            + stats.PlantWaterNeed >= PlantWaterNeedConfig.CriticalValue ? PlantAnimicModifier : 0
            + stats.DogWalkNeed >= DogWalkNeedConfig.CriticalValue ? DogAnimicModifier : 0
            + TireingAnimicModifier * (stats.Tireing >=  TireingConfig.CriticalValue ? 1 : -1)
            + (stats.DishesWashed ? 0 : DirtyDishesAnimicModifier);
        stats.AnimicState += modifier * deltaTime;
        DogWalkNeedConfig.Tick(deltaTime, ref stats.DogWalkNeed);
        PlantWaterNeedConfig.Tick(deltaTime, ref stats.PlantWaterNeed);
    }
}

[System.Serializable]
public class NeedConfig {
    public float Speed;
    public float CriticalValue;
    public float Max;

    public void Tick(float deltaTime, ref float value) {
        value += Speed * deltaTime;
        if (value > Max) {
            value = Max;
        }
    }
}
