using UnityEngine;

[CreateAssetMenu(fileName = "SceneList", menuName = "Asset/Scene List", order = 0)]
public class SceneList : ScriptableObject {
    static SceneList inst;
    public static SceneList Inst {
        get {
            if (inst == null) {
                inst = Resources.Load<SceneList>("SceneList");
            }
            return inst;
        }
    }

    [System.Serializable]
    public class ScenePair {
        public SceneHelper.Scenes Scene;
        public Object Asset;
        [HideInInspector]
        public int SceneIndex;
    }

    public ScenePair[] Scenes;

    public static int GetSceneIndex(SceneHelper.Scenes scene) {
        for (int i = 0; i < Inst.Scenes.Length; i++) {
            if (Inst.Scenes[i].Scene == scene) {
                return Inst.Scenes[i].SceneIndex;
            }
        }
        return -1;
    }
}
