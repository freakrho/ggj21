using UnityEngine;
using UnityEditor;
using System;

[InitializeOnLoad]
public class SceneListEditorProcess {
    static SceneListEditorProcess() {
        EditorBuildSettings.sceneListChanged += OnChangeSceneList;
    }

    private static void OnChangeSceneList() {
        var assets = AssetDatabase.FindAssets("t:SceneList");
        foreach (var guid in assets) {
            SceneListEditor.AssignSceneIndices(
                AssetDatabase.LoadAssetAtPath<SceneList>(
                    AssetDatabase.GUIDToAssetPath(guid)
                )
            );
        }
    }
}
