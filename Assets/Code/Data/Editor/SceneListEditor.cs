using UnityEngine;
using UnityEditor;

[CustomEditor(typeof(SceneList))]
public class SceneListEditor : Editor {
    public override void OnInspectorGUI() {
        EditorGUI.BeginChangeCheck();
        base.OnInspectorGUI();
        if (GUILayout.Button("Force Update")) {
            AssignSceneIndices(target as SceneList);
        }
        if (EditorGUI.EndChangeCheck()) {
            AssignSceneIndices(target as SceneList);
        }
    }

    static int FindSceneIndex(Object asset) {
        int index = 0;
        for (int i = 0; i < EditorBuildSettings.scenes.Length; i++) {
            var scene = EditorBuildSettings.scenes[i];
            if (scene.path == AssetDatabase.GetAssetPath(asset)) {
                return index;
            }
            if (scene.enabled) {
                index++;
            }
        }
        return -1;
    }

    public static void AssignSceneIndices(SceneList sceneList) {
        foreach (var item in sceneList.Scenes) {
            item.SceneIndex = FindSceneIndex(item.Asset);
        }
        EditorUtility.SetDirty(sceneList);
    }
}
