using System;
using System.Collections.Generic;
using TwitchLib.Client.Events;
using UnityEngine;

public class ReactionSpawner : MonoBehaviour {
    static readonly char[] SEPARATORS = {' ', '.', ',', '!', ':', ';'};
    public ParticleSystem GoodReaction;
    public ParticleSystem BadReaction;
    public TextAsset Words;
    public Collider2D SpawnRange;

    string[] good;
    string[] bad;

    private void Start() {
        if (!TwitchClient.Online) return;

        TwitchClient.Client.OnMessageReceived += OnMessageReceived;

        var lines = Words.text.Split('\n', '\r');
        var goodWords = new List<string>();
        var badWords = new List<string>();
        for (int i = 1; i < lines.Length; i++) {
            var words = lines[i].Split(',', ';');
            if (words.Length > 0) {
                goodWords.Add(words[0].Trim().ToLower());
            }
            if (words.Length > 1) {
                badWords.Add(words[1].Trim().ToLower());
            }
        }
        good = goodWords.ToArray();
        bad = badWords.ToArray();
    }

    void SpawnParticles(ParticleSystem ps) {
        var bounds = SpawnRange.bounds;
        var position = new Vector3(
            UnityEngine.Random.Range(bounds.min.x, bounds.max.x),
            UnityEngine.Random.Range(bounds.min.y, bounds.max.y),
            UnityEngine.Random.Range(bounds.min.z, bounds.max.z)
        );
        ps.transform.position = position;
        var emitParams = new ParticleSystem.EmitParams();

        ps.Emit(emitParams, 1);

    }

    void SpawnGood() {
        PlayerStats.Inst.Stats.AnimicState -= PlayerRoom.Inst.StatsConfig.GoodCommentBoost;
        SpawnParticles(GoodReaction);
    }

    void SpawnBad() {
        PlayerStats.Inst.Stats.AnimicState -= PlayerRoom.Inst.StatsConfig.NegativeCommentBoost;
        SpawnParticles(BadReaction);
    }

    private void OnMessageReceived(object sender, OnMessageReceivedArgs e) {
        var words = e.ChatMessage.Message.Split(SEPARATORS);
        foreach (var word in words) {
            if (good.Contains(word)) {
                SpawnGood();
                return;
            } else if (bad.Contains(word)) {
                SpawnBad();
                return;
            }
        }
    }
}
