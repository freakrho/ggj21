using UnityEngine;
using TwitchLib.Client.Models;
using TwitchLib.Unity;
using TwitchLib.Client.Events;
using System;

public class TwitchClient : MonoBehaviour {
    static TwitchClient inst;
    public static TwitchClient Inst {
        get {
            if (inst == null) {
                inst = FindObjectOfType<TwitchClient>();
            }
            return inst;
        }
    }
    public static Client Client { get { return Inst.client; } }
    public static bool Online { get { return inst != null && Client != null; } }

    Client client;
    public string ChannelName;

    private void Awake() {
        if (inst != null && inst != this) {
            Destroy(gameObject);
            return;
        }

        DontDestroyOnLoad(gameObject);
        inst = this;

    }

    public void Init() {
        // Init
        Application.runInBackground = true;
        ConnectionCredentials credentials = new ConnectionCredentials(
            TwitchSecrets.Bot.USERNAME,
            TwitchSecrets.Bot.ACCESS_TOKEN
        );
        client = new Client();
        client.Initialize(credentials, ChannelName);

        client.Connect();
        client.OnConnected += OnConnected;
    }

    private void OnConnected(object sender, OnConnectedArgs e) {
        client.SendMessage(client.JoinedChannels[0], "I'm connected :)");
    }
}
