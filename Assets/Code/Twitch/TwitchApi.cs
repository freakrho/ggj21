using UnityEngine;
using TwitchLib.Unity;

public class TwitchApi : MonoBehaviour {
    static TwitchApi inst;
    public static TwitchApi Inst {
        get {
            if (inst == null) {
                inst = FindObjectOfType<TwitchApi>();
            }
            return inst;
        }
    }
    public static Api Api { get { return Inst.api; } }

    Api api;

    private void Awake() {
        if (inst != null && inst != this) {
            Destroy(gameObject);
            return;
        }
        DontDestroyOnLoad(gameObject);
        inst = this;

        api = new Api();
        api.Settings.AccessToken = TwitchSecrets.Client.SECRET;
        api.Settings.ClientId = TwitchSecrets.Client.ID;
    }
}
