using System;
using System.IO;
using TMPro;
using TwitchLib.Client.Events;
using UnityEngine;
using UnityEngine.UI;

public class TwitchController : MonoBehaviour {
    public TMP_InputField Input;
    public Color ConnectedColor = Color.green;
    public Image ConnectedImage;
    
    string SavedUsername {
        get {
            var path = Path.Combine(Application.persistentDataPath, "username.txt");
            if (File.Exists(path)) {
                return File.ReadAllText(path);
            }
            return "";
        }
        set {
            File.WriteAllText(Path.Combine(Application.persistentDataPath, "username.txt"), value);
        }
    }

    private void Start() {
        Input.text = SavedUsername;
        if (Input.text != "") {
            Init();
        }
    }

    public void Init() {
        TwitchClient.Inst.ChannelName = Input.text;
        TwitchClient.Inst.Init();
        TwitchClient.Client.OnConnected += OnConnected;
    }

    private void OnConnected(object sender, OnConnectedArgs e) {
        SavedUsername = Input.text;
        ConnectedImage.color = ConnectedColor;
    }
}
