using PixelCrushers.DialogueSystem;
using UnityEngine;
using UnityEngine.InputSystem;

public class Player : MonoBehaviour {
    const float THRESHOLD = 0.01f;
    static readonly int DIR_X = Animator.StringToHash("DirX");
    static readonly int DIR_Y = Animator.StringToHash("DirY");
    static readonly int VEL_X = Animator.StringToHash("VelX");
    static readonly int VEL_Y = Animator.StringToHash("VelY");
    static readonly int VEL = Animator.StringToHash("Vel");
    static readonly int ACTION = Animator.StringToHash("Action");

    public float WalkSpeed;
    public float InteractionDistance;
    public LayerMask InteractionMask;
    public Animator Animator;
    public Vector2 InteractionOffset;

    enum State {
        Idle,
        Walking,
    }
    State state;
    Vector2 move;
    Vector2 direction;
    float stateTime;
    Rigidbody2D rb;
    Usable selectedUsable;

    private void Awake() {
        rb = GetComponent<Rigidbody2D>();
    }

    private void Start() {
        SetState(State.Idle);
    }

    public void OnMove(InputValue input) {
        move = input.Get<Vector2>();
    }

    void SetState(State newState) {
        state = newState;
        stateTime = 0;
        // Debug.Log($"Set state {state}");

        if (state == State.Idle) {
            rb.velocity = Vector2.zero;
        }
    }

    private void Update() {
        if (PlayerRoom.Inst.Pause) {
            rb.velocity = Vector2.zero;
            return;
        }

        if (state == State.Idle) {
            if (move.sqrMagnitude >= THRESHOLD) {
                SetState(State.Walking);
                return;
            }
        } else if (state == State.Walking) {
            rb.velocity = move * WalkSpeed;
            if (move.sqrMagnitude <= THRESHOLD) {
                SetState(State.Idle);
                return;
            }
            if (Mathf.Abs(move.x) > Mathf.Abs(move.y)) {
                direction = Vector2.right * Mathf.Sign(move.x);
            } else {
                direction = Vector2.up * Mathf.Sign(move.y);
            }
        }

        FindUsables();

        stateTime += Time.deltaTime;
    }

    private void FixedUpdate() {
        Animator.SetFloat(DIR_X, direction.x);
        Animator.SetFloat(DIR_Y, direction.y);
        Animator.SetFloat(VEL_X, rb.velocity.x);
        Animator.SetFloat(VEL_Y, rb.velocity.y);
        Animator.SetFloat(VEL, rb.velocity.sqrMagnitude);
    }

    void OnInteract(InputValue input) {
        if (PlayerRoom.Inst.Pause) return;
        
        UseSelectedUsable();
    }

    void OnPause(InputValue input) {
        if (!PlayerRoom.Inst.PauseMenu.Visible) {
            PlayerRoom.Inst.PauseMenu.EnterPause();
        } else {
            PlayerRoom.Inst.PauseMenu.ExitPause();
        }
    }

    void FindUsables() {
        var output = Physics2D.RaycastAll(
            (Vector2) transform.position + InteractionOffset,
            direction,
            InteractionDistance,
            InteractionMask
        );
        Debug.DrawRay((Vector2) transform.position + InteractionOffset,
            direction * InteractionDistance);
        for (int i = 0; i < output.Length; i++) {
            var col = output[i];
            if (col.collider != null) {
                var usable = col.collider.gameObject.GetComponent<Usable>();
                if (usable.enabled) {
                    SelectUsable(usable);
                    return;
                }
            }
        }
        DeselectUsable();
    }

    void DeselectUsable() {
        if (selectedUsable != null) {
            selectedUsable.OnDeselectUsable();
            selectedUsable = null;
        }
    }

    void SelectUsable(Usable usable) {
        if (usable == selectedUsable) return;
        
        DeselectUsable();
        usable.OnSelectUsable();
        selectedUsable = usable;
        Debug.Log($"Selected {usable.name}", usable);
    }
    
    void UseSelectedUsable() {
        if (selectedUsable == null) return;

        Animator.SetTrigger(ACTION);
        selectedUsable.OnUseUsable();
    }
}
