using System.Collections;
using Doozy.Engine.UI;
using UnityEngine;
using UnityEngine.SceneManagement;

public class Splash : MonoBehaviour {
    public UIView[] ViewSequence;
    public float holdTime = 2f;

    IEnumerator Start() {
        for (int i = 0; i < ViewSequence.Length; i++) {
            ViewSequence[i].Hide(true);
        }

        // Load loading scene
        var loadingOper = SceneHelper.LoadSceneAsyncRaw(
            SceneHelper.Scenes.Loading, LoadSceneMode.Additive);

        // View sequence
        for (int i = 0; i < ViewSequence.Length; i++) {
            var view = ViewSequence[i];
            view.Show();
            while (view.Visibility != VisibilityState.Visible) yield return null;
            yield return new WaitForSecondsRealtime(holdTime);
            view.Hide();
            while (view.Visibility != VisibilityState.NotVisible) yield return null;
        }
        
        var roomOper = SceneHelper.LoadSceneAsyncRaw(
            SceneHelper.Scenes.Room, LoadSceneMode.Additive);
        
        while (!roomOper.isDone && !loadingOper.isDone) yield return null;
        LoadingScene.HideInstant();

        SceneManager.UnloadSceneAsync(gameObject.scene);
    }
}
