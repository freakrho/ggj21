using UnityEngine;
using UnityEngine.InputSystem;

public class Game : MonoBehaviour {
    public static Game Inst { get; private set; }

    public enum ControllerType {
        Keyboard,
        OnScreen,
    }
    ControllerType controller;
    public static ControllerType Controller => Inst.controller;

    private void Awake() {
        if (Inst != null && Inst != this) {
            DestroyImmediate(gameObject);
            return;
        }
        DontDestroyOnLoad(gameObject);
        Inst = this;
    }

    private void Start() {
        SetControllerType(ControllerType.Keyboard);
    }

    void SetControllerType(ControllerType controllerType) {
        Debug.Log($"Controller type: {controllerType}");
        controller = controllerType;
        if (controller == ControllerType.OnScreen) {
            if (OnScreenControls.Inst != null) {
                OnScreenControls.Show();
            }
        } else {
            if (OnScreenControls.Inst != null) {
                OnScreenControls.Hide();
            }
        }
    }

    void OnAnyTouch(InputValue input) {
        if (controller != ControllerType.OnScreen && input.isPressed) {
            SetControllerType(ControllerType.OnScreen);
        }
    }

    void OnAnyKey(InputValue input) {
        if (controller != ControllerType.Keyboard && input.isPressed) {
            SetControllerType(ControllerType.Keyboard);
        }
    }

    void OnAnyMouseClick(InputValue input) {
        if (controller != ControllerType.Keyboard && input.isPressed) {
            SetControllerType(ControllerType.Keyboard);
        }
    }
}
