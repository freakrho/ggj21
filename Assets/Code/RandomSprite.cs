using UnityEngine;

public class RandomSprite : MonoBehaviour {
    public Sprite[] Sprites;

    private void Start() {
        var renderer = GetComponent<SpriteRenderer>();
        renderer.sprite = Sprites[Random.Range(0, Sprites.Length)];
    }
}
