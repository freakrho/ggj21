using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SoundManager : MonoBehaviour{
    static SoundManager inst;
    public AudioSource EffectsSource;
	public AudioSource MusicSource;

    public AudioClip AmbienceMusic;

    public static SoundManager Inst {
        get {
            if (inst == null) {
                inst = FindObjectOfType<SoundManager>();
            }
            if (inst == null) {
                inst = new GameObject("SoundManager").AddComponent<SoundManager>();
            }
            return inst;
        }
    }

    private void Start() {
        SoundManager.Inst.PlayMusic(AmbienceMusic);
    }
    void Awake(){
		DontDestroyOnLoad (gameObject);
    }
    
    public void Play(AudioClip clip)
	{
		EffectsSource.clip = clip;
		EffectsSource.Play();
	}

	public void PlayMusic(AudioClip clip)
	{
		MusicSource.clip = clip;
        MusicSource.loop = true;
		MusicSource.Play();
	}

}
